from django.apps import AppConfig


class TweezerAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tweezer_app'
