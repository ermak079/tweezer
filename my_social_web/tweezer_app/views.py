from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, HttpRequest, HttpResponseForbidden
from django.urls import reverse
from django.contrib.auth.hashers import make_password, check_password
from django.core.exceptions import PermissionDenied
from tweezer_app.models import *
from datetime import datetime, date, timedelta
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
import re
from collections import Counter


def auth(request, link):
    if request.session.get('auth', False):
        me = request.session.get('user_id')
        user = User_info.objects.get(pk=me)
        user_following = user.following.all()
        following = []
        for us in user_following:
            following.append(us.following_user_id)
        blog = BlogPosts.objects.filter(user_id=me).order_by('-date')
        all_blog = BlogPosts.objects.all().order_by('-date')
        max_count_hashtag = popular_hashtag(request)
        for post in all_blog:  # цикл по всем постам в БД
            post.liked_by_current_user = post.is_liked_by(me)
            post.user.is_follow_by_current_user = post.user.is_it_follow(me)
        return render(request, link, {
            'user': user,
            'user_following': following,
            'is_auth': request.session.get('auth'),
            'blog': blog,
            'all_blog': all_blog,
            'max_count_hashtag': max_count_hashtag
        })
    else:
        raise PermissionDenied


def authorization(request):
    me = request.session.get('user_id')
    if me:
        user = User_info.objects.get(pk=me)
    is_error = False
    if request.method == "POST":
        login = request.POST['login']
        login_password = request.POST['password']
        try:
            u = User_info.objects.get(user=login)
        except User_info.DoesNotExist:
            u = False
        if u:
            if check_password(login_password, u.password):
                request.session['auth'] = True
                request.session['user_id'] = u.id
                redirect_url = reverse('main_page', )
                return HttpResponseRedirect(redirect_url)
            else:
                is_error = True
        else:
            is_error = True
    if me:
        return render(request, 'tweezer_app/index_authorization.html', {
            'is_error': is_error,
            'user': user
        })
    else:
        return render(request, 'tweezer_app/index_authorization.html', {
            'is_error': is_error
        })



def registration(request):
    if request.method == "POST":
        a = User_info()
        a.email = request.POST['email']
        a.user = request.POST['login']
        a.password = make_password(request.POST['password'])
        a.save()
    return render(request, 'tweezer_app/index_registration.html')


def main_page(request):
    all_blog = BlogPosts.objects.all().order_by('-date')
    hashtags = Hashtag.objects.all().order_by('-post')
    max_count_hashtag = popular_hashtag(request)
    if request.session.get('auth', False):
        me = request.session.get('user_id')
        add_like(request)
        delete_post(request)
        subscribe(request)
        user = User_info.objects.get(pk=me)
        for post in all_blog:  # цикл по всем постам в БД
            post.liked_by_current_user = post.is_liked_by(me)
            post.user.is_follow_by_current_user = post.user.is_it_follow(me)
        return render(request, 'tweezer_app/main_page.html', {
            'user': user,
            'is_auth': request.session.get('auth'),
            'all_blog': all_blog,
            'max_count_hashtag': max_count_hashtag
        })
    else:
        return render(request, 'tweezer_app/main_page.html', {
            'all_blog': all_blog,
            'max_count_hashtag': max_count_hashtag
        })


def show_users_info(request, id: int):
    users = get_object_or_404(User_info, pk=id)
    blog = BlogPosts.objects.filter(user_id=users.id).order_by('-date')
    max_count_hashtag = popular_hashtag(request)
    if request.session.get('auth', False):
        me = request.session.get('user_id')
        user = User_info.objects.get(pk=me)
        add_like(request)
        delete_post(request)
        for post in blog:  # цикл по всем постам в БД для проверки наличия лайка от юзера
            post.liked_by_current_user = post.is_liked_by(me)
            post.user.is_follow_by_current_user = post.user.is_it_follow(me)
        return render(request, 'tweezer_app/users.html', {
            'user': user,
            'users': users,
            'is_auth': request.session.get('auth'),
            'blog': blog,
            'max_count_hashtag': max_count_hashtag
        })
    else:
        user = User_info.objects.get(pk=users.id)
        return render(request, 'tweezer_app/users.html', {
            'user': user,
            'users': users,
            'is_auth': request.session.get('auth'),
            'blog': blog,
            'max_count_hashtag': max_count_hashtag
        })


def show_user_info(request):
    post_blog(request)
    add_like(request)
    delete_post(request)
    return auth(request, 'tweezer_app/user_info.html')


def delete_post(request):
    post_id = request.POST.get('delete_blog_id')
    if post_id:
        blog = BlogPosts.objects.get(pk=post_id)
        hashtag = Hashtag.objects.filter(post=post_id)
        for i in hashtag:
            hashtag.delete()
        blog.delete()


def post_blog(request):
    me = request.session.get('user_id')
    user = User_info.objects.get(pk=me)
    blog = BlogPosts()
    all_users = User_info.objects.all()
    request_files = request.FILES.get('upload')
    request_text = request.POST.get('text_blog')
    if request_files or request_text:
        if request.FILES.get('upload'):
            upload = request.FILES['upload']
            fss = FileSystemStorage()
            file = fss.save(upload.name, upload)
            file_url = fss.url(file)
            blog.img = file_url
        if request.POST.get('text_blog'):
            blog.text = request.POST['text_blog']
        blog.user_id = me
        for us in all_users:
            if us.user in blog.text:
                link_user = f'<a href="/tweezer/user/{us.pk}">@{us.user}</a>'
                blog.text = re.sub(f'@{us.user}', link_user, blog.text)
        blog.date = datetime.now()
        blog.save()
        if '#' in blog.text:
            tag = re.findall(r'#\w{1,30}[^#]', blog.text)
            tag_set = set(tag)
            new_tag = list(tag_set)
            for i in new_tag:
                hashtag = Hashtag()
                hashtag.text = str.strip(i[1:])
                hashtag.user = user
                hashtag.post = blog
                hashtag.date = datetime.now()
                hashtag.save()
                link_hashtag = f'<a href="/tweezer/{hashtag.text}">#{hashtag.text}</a>'
                blog.text = re.sub('#' + hashtag.text, link_hashtag, blog.text)
            blog.save()
        redirect_url = reverse('me', )
        return HttpResponseRedirect(redirect_url)


def logout(request):
    del request.session['auth']
    return HttpResponseRedirect('/tweezer')


def settings(request):
    me = request.session.get('user_id')
    user = User_info.objects.get(pk=me)
    is_error = 0
    if request.method == 'POST' and request.FILES.get('upload'):
        upload = request.FILES['upload']
        fss = FileSystemStorage()
        file = fss.save(upload.name, upload)
        file_url = fss.url(file)
        user.img = file_url
        user.save()
        return render(request, 'tweezer_app/settings.html', {'user': user})
    elif request.method == 'POST':
        old_pass = request.POST['old_pass']
        new_pass = request.POST['new_pass']
        verification = request.POST['verification_new_pass']
        if check_password(old_pass, user.password):
            if new_pass == verification:
                user.password = make_password(new_pass)
                user.save()
                is_error = 1
            else:
                is_error = 2
        else:
            is_error = 3

        return render(request, 'tweezer_app/settings.html', {'user': user,
                                                             'is_error': is_error
                                                             })
    return auth(request, 'tweezer_app/settings.html')


def add_like(request):
    blog_id = request.GET.get('like')
    if blog_id:
        me = request.session.get('user_id')
        user = User_info.objects.get(pk=me)
        blog = BlogPosts.objects.get(pk=blog_id)
        liked_users = blog.liked_users.filter(id=me)  # переменная куда помещаются все лайки от user
        if len(liked_users) == 0:  # проверка по длине списка. Если равно 0 то добавляем user.id в liked_users
            blog.liked_users.add(user.id)
            blog.save()
        else:  # если больше 0, то удаляем запись из liked_users
            blog.liked_users.remove(user.id)
            blog.save()


def subscribe(request):
    follow = request.GET.get('user_to_subscribe')
    if follow:
        me = request.session.get('user_id')
        user = User_info.objects.get(pk=me)
        following_user = User_info.objects.get(pk=follow)
        all_following_user = UserFollowing.objects.filter(user_id=me,
                                                          following_user_id=following_user)
        if len(all_following_user) == 0:
            UserFollowing.objects.create(user_id=user,
                                         following_user_id=following_user)
        else:
            UserFollowing.objects.filter(user_id=user,
                                         following_user_id=following_user).delete()


def my_feed(request):
    add_like(request)
    subscribe(request)
    return auth(request, 'tweezer_app/my_feed.html')


def show_user_followers(request, id: int):
    me = request.session.get('user_id')
    user = User_info.objects.get(pk=me)
    user_follower = User_info.objects.get(pk=id)
    user_followers = user_follower.followers.all()
    max_count_hashtag = popular_hashtag(request)
    followers = []
    for us in user_followers:
        followers.append(us.user_id)
    return render(request, 'tweezer_app/user_followers.html', {
        'user': user,
        'followers': followers,
        'is_auth': request.session.get('auth'),
        'max_count_hashtag': max_count_hashtag
    })


def show_user_following(request, id: int):
    me = request.session.get('user_id')
    user = User_info.objects.get(pk=me)
    users_following = User_info.objects.get(pk=id)
    user_followings = users_following.following.all()
    max_count_hashtag = popular_hashtag(request)
    following = []
    for us in user_followings:
        following.append(us.following_user_id)
    return render(request, 'tweezer_app/user_following.html', {
        'user': user,
        'following': following,
        'is_auth': request.session.get('auth'),
        'max_count_hashtag': max_count_hashtag
    })


def show_all_posts_a_hashtag(request, hashtag: str):
    me = request.session.get('user_id')
    user = User_info.objects.get(pk=me)
    add_like(request)
    subscribe(request)
    posts_a_hashtag = Hashtag.objects.filter(text=hashtag).order_by('-post')
    hashtag_post = []
    max_count_hashtag = popular_hashtag(request)
    for i in posts_a_hashtag:
        blog = BlogPosts.objects.get(pk=i.post.pk)
        hashtag_post.append(blog)
    for post in hashtag_post:  # цикл по всем постам в БД
        post.liked_by_current_user = post.is_liked_by(me)
        post.user.is_follow_by_current_user = post.user.is_it_follow(me)
    return render(request, 'tweezer_app/hashtag.html', {
        'hashtag_post': hashtag_post,
        'user': user,
        'max_count_hashtag': max_count_hashtag
    })


def popular_hashtag(request):
    start_date = datetime.today()
    end_date = start_date - timedelta(days=1)
    hashtags = Hashtag.objects.filter(date__range=[end_date, start_date])
    text_hashtags = []
    for tag in hashtags:
        text_hashtags.append(tag.text)
    hashtag_count = Counter(text_hashtags)
    max_count_hashtag = hashtag_count.most_common(5)
    return max_count_hashtag

