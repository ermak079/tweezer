let ButtonFormAttach = document.getElementById('button_form_attach');
let ButtonFormDeleteAttach = document.getElementById('button_form_delete_attach');
let UploadFileBlog = document.getElementById('upload_file_blog');
let up = document.getElementById('upload_file_blog_text')

if( UploadFileBlog ){
    UploadFileBlog.addEventListener('change', function(){
      if( this.value ){
        ButtonFormAttach.classList.toggle('hidden')
        ButtonFormDeleteAttach.classList.toggle('hidden')
        up.innerText = '📎 ' + UploadFileBlog.value.replace(/.*[\\\/]/, "")
      }
    });
}

if ( ButtonFormDeleteAttach ){
    ButtonFormDeleteAttach.onclick = function(){
          if( UploadFileBlog.value ){
            UploadFileBlog.value=''
            ButtonFormAttach.classList.toggle('hidden')
            ButtonFormDeleteAttach.classList.toggle('hidden')
            up.innerText = ''
            }
    }
}



