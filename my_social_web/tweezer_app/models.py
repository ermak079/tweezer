from django.db import models


# Create your models here.


class User_info(models.Model):
    user = models.CharField(max_length=40)
    email = models.EmailField(max_length=40)
    password = models.CharField(max_length=60)
    img = models.CharField(max_length=100, default='/static/tweezer_app/img/gus.jpg')
    is_follow_by_current_user = False

    def is_it_follow(self, user_id):
        user = self.followers.filter(user_id=user_id)
        if len(user) == 0:
            return False
        else:
            return True


class BlogPosts(models.Model):
    text = models.CharField(max_length=300)
    date = models.DateTimeField()
    user = models.ForeignKey(User_info, on_delete=models.DO_NOTHING)
    liked_users = models.ManyToManyField(User_info, related_name='likes_blog_post')
    img = models.CharField(max_length=100, default='')
    liked_by_current_user = False  # свойство(переменная) модели

    def is_liked_by(self, user_id):  # метод для проверки лайков на выбранном посте от найденного пользователя
        liked_users = self.liked_users.filter(id=user_id)
        if len(liked_users) == 0:  # возвращает False если нет лайков от пользователя, и True если есть
            return False
        else:
            return True


class UserFollowing(models.Model):
    user_id = models.ForeignKey(User_info, related_name="following", on_delete=models.DO_NOTHING)
    following_user_id = models.ForeignKey(User_info, related_name="followers", on_delete=models.DO_NOTHING)


class Hashtag(models.Model):
    user = models.ForeignKey(User_info, on_delete=models.SET_NULL, null=True)
    post = models.ForeignKey(BlogPosts, on_delete=models.SET_NULL, null=True)
    text = models.CharField(max_length=300)
    date = models.DateTimeField(null=True)








