from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.authorization, name='tweezer'),
    path('registration', views.registration),
    path('main_page', views.main_page, name='main_page'),
    path('user/<int:id>', views.show_users_info),
    path('user/<int:id>/followers', views.show_user_followers),
    path('user/<int:id>/following', views.show_user_following),
    path('me', views.show_user_info, name='me'),
    path('logout', views.logout),
    path('settings', views.settings, name='set'),
    path('my_feed', views.my_feed),
    path('<str:hashtag>', views.show_all_posts_a_hashtag),
    ]
